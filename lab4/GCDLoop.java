public class GCDLoop {

	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);

		//int q = a / b;
		int r = a % b;
		System.out.println("GCD (" + a + ", " + b  + ")");
		
		while(r != 0) {
			a = b;
			b = r;
			//q = a / b;
			r = a % b;
		}
		
		System.out.println("GCD is " + b);
	}

}
