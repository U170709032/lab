package lab;

public class Main {

	public static void main(String[] args) {
		Rectangle Rectangle = new Rectangle();
		Rectangle.sideA = 5;
		Rectangle.sideB = 6;
		int sideA = 5;
		int sideB = 6;
		System.out.println(lab.Rectangle.area(sideA, sideB));
		System.out.println(lab.Rectangle.perimeter(sideA, sideB));
		
		Circle Circle = new Circle();
		Circle.radius = 10;
		int radius = 10;
		System.out.println(lab.Circle.area(radius));
		System.out.println(lab.Circle.perimeter(radius));
	}

}
