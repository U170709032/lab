package lab;

public class Circle {
	int radius;
	
	public static int area(int radius) {
		int area = 3 * (radius*radius);
		return area;	
	}
	
	public static int perimeter(int radius) {
		int perimeter = 2 * 3 * radius;
		return perimeter;	
	}
	
}
