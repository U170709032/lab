package lab;

public class Rectangle {
	int sideA;
	int sideB;
	
	public static int area(int sideA, int sideB) {
		int area = sideA * sideB;
		return area;	
	}
	
	public static int perimeter(int sideA, int sideB) {
		int perimeter = 2 * (sideA + sideB);
		return perimeter;	
	}
	
}
