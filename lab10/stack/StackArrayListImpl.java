package stack;

import java.util.ArrayList;

public class StackArrayListImpl<T> implements Stack<T> {
	private ArrayList<E> contents = new ArrayList<T>();
	
	@Override 
	public void push(T item) {
		contents.add(item);
	}
	
	@Override
	public T pop() {
		return (T) contents.remove(contents.size() -1);
	}
	
	@Override
	public boolean empty() {
		return contents.size() == 0;
	}
	
}
