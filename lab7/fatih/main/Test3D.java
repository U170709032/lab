package fatih.main;

import fatih.shapes3d.Cube;
import fatih.shapes3d.Cylinder;

public interface Test3D {
	
	public static void main(String[] args) {
		Cylinder cylinder = new Cylinder(6, 10);
		Cube cube = new Cube(5);
		
		System.out.println(cylinder.toString());
		System.out.println(cylinder.area());
		System.out.println(cylinder.volume());
		System.out.println(cube.toString());
		System.out.println(cube.area());
		System.out.println(cube.volume());
		
		
	}
		

}
