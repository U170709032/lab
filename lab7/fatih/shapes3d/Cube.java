package fatih.shapes3d;

public class Cube {
	int sides;
	
	public Cube(int sides) {
		this.sides = sides;
	}
	
	public int area() {
		return sides*sides;
	}
	
	public int volume() {
		return sides*sides*sides;
	}
	
	public String toString() {
		return "Sides are = " + sides;
	}
	
}
